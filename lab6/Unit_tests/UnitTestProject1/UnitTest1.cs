﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Unit_tests;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void silnia_0()
        {
            silnia sl = new silnia();
            sl.n = 0;
            Assert.AreEqual(sl.oblicz(), 1);
        }
        [TestMethod]
        public void silnia_1()
        {
            silnia sl = new silnia();
            sl.n = 1;
            Assert.AreEqual(sl.oblicz(), 9);
        }
        [TestMethod]
        public void silnia_5()
        {
            silnia sl = new silnia();
            sl.n = 5;
            Assert.AreEqual(sl.oblicz(), 120);
        }
    }
}
