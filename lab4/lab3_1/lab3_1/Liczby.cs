﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace lab3_1
{
    public class Liczby
    {
        protected int a = 0;
        protected int b = 0;
        protected int c = 0;

        public Liczby()
        {
            a = 0;
            b = 0;
            c = 0;
        }

        public int getA()
        {
            return a;
        }
        public int getB()
        {
            return b;
        }
        public int getC()
        {
            return c;
        }
        public void setA(int a)
        {
            this.a = a;
        }
        public void setB(int b)
        {
            this.b = b;
        }
        public void setC(int c)
        {
            this.c = c;
        }
    }
    
}